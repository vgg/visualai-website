## Agenda

**10.00 - 10.10**: Introduction

**10.10 - 10.30**: Face Recognition

**10.30 - 11.00**: Image and video annotation

**11.00 - 11.20**: Coffee break

**11.20 - 11.40**: Tracking in Video

**11.40 - 12.00**: Image and video search

**12.00 - 12.15**: Digital humanities examples

**12.15 - 12.30**: Next steps

**12.30 - 14.00**: Lunch Break

**14.00 - 15.00**: Consultations


## Contact

[Contact the Organiser](mailto:visualai.uk@gmail.com)