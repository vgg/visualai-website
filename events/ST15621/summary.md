## Description

The purpose of the Show and Tell is to demonstrate computer vision software that is able to analyse, describe and search image and video content with human-like capabilities. The software also includes tracking in videos and annotations of images and videos.

All the software is open source and at the cutting edge of AI.  It is potentially transformational for research across many disciplines (such as Archaeology, Art, Geology, Medicine, Plant sciences and Zoology).

## Presentations

[Introduction - Andrew Zisserman and Alison Noble](events/ST15621/data/intro.pdf)
[Face Recognition - Andrew Zisserman](events/ST15621/data/face_recognition_az.pdf)
[Image and Video Annotation - Abhishek Dutta](events/ST15621/data/via_adutta.pdf)
[Tracking in Video - Andrea Vedaldi (Coming soon)](#ST15621)
[Tracking in Video demo - Prasanna Sridhar](events/ST15621/data/tracking_demo.pdf)
[Image and Video Search - Andrew Zisserman](events/ST15621/data/image_and_video_search_az.pdf)
[VGG Image Search Engine (VISE) demo - Abhishek Dutta](events/ST15621/data/vise_adutta.pdf)
[Digital Humanities examples - Giles Bergel](events/ST15621/data/digital_humanities_examples_gbergel.pdf)
[Software release, Support and next steps](events/ST15621/data/wrap_up.pdf)

## Presenters

Dr Giles Bergel, Dr Abhishek Dutta, Prasanna Sridhar, Professor Alison Noble, Professor Andrea Vedaldi, Professor Andrew Zisserman