## Agenda

**10.00 - 10.10**: Introduction

**10.10 - 10.30**: VIA - Image and video annotation

**10.30 - 11.00**: Tracking in video

**11.00 - 11.20**: Image Comparison

**11.20 - 11.35**: Coffee break

**11.35 - 12.00**: Image search and retrieval

**12.00 - 12.20**: Segmentation methods

**12.20 - 12.40**: Wearable Sensors

**12.40 - 12.50**: Class Agnostic Counting

**12.50 - 13.00**: Wrap up

## Contact

[Contact the Organiser](mailto:visualai.uk@gmail.com)