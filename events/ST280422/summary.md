## Description

The purpose of the Show and Tell is to demonstrate computer vision methods to search, track, segment, and compare image and video content. The methods are available as open source and supported computer vision software. The software also includes annotation tools for images and videos.


## Presentations

[Introduction - Andrew Zisserman and Alison Noble](https://drive.google.com/file/d/1eHIki_sbwgeDD2zC9CW3-nWxd8T_8RLJ/view?usp=sharing)
[VIA - Image and Video Annotation - Abhishek Dutta](https://drive.google.com/file/d/1OjcsQ-RU5ELxAsyc7JaDXXn12RLh_iLB/view?usp=sharing)
[Tracking in Video - Andrea Vedaldi](https://drive.google.com/file/d/1AVWiG1czYeggE-MKhWEX73n4mF638-PQ/view?usp=sharing)
[Tracking in Video demo - Prasanna Sridhar](https://drive.google.com/file/d/1Mu4DEwmEf9l8MK4uqyt_4k9oJrUA0ItH/view?usp=sharing)
[Image Comparison - Prasanna Sridhar](https://drive.google.com/file/d/1Wvexy5uZQbMRpeprPr6LfhZjEvevHOUm/view?usp=sharing)
[Image and Video Search - Andrew Zisserman](https://drive.google.com/file/d/16zwvKUBDGaMINl2oCJ0vzsbM_FmkqWZr/view?usp=sharing)
[VGG Image Search Engine (VISE) demo - Abhishek Dutta](https://drive.google.com/file/d/1F8acPetV6BNhDNCu1j1_TNiJ90cYEL6e/view?usp=sharing)
[PixelPick Segmentation - Weidi Xie](https://www.dropbox.com/s/l3l4qcrnj5tw7o8/Show%20%26%20Tell%20-%20Weidi.pdf?dl=0)
[Self-supervised Tumour Segmentation - Weidi Xie (Slide 17)](https://www.dropbox.com/s/l3l4qcrnj5tw7o8/Show%20%26%20Tell%20-%20Weidi.pdf?dl=0)
[Wearable Sensors - Dima Damen](https://uob-my.sharepoint.com/:b:/g/personal/csxda_bristol_ac_uk/EXPd1NoE0lJIr4wYTiis6F4BGcOTW31sZHS722SBWhzbDQ?e=pdKoZj)
[Class Agnostic Counting - Weidi Xie (Slide 29)](https://www.dropbox.com/s/l3l4qcrnj5tw7o8/Show%20%26%20Tell%20-%20Weidi.pdf?dl=0)
[Pixee - Abhishek Dutta](https://drive.google.com/file/d/1nvYhNKgGAFXhgSFNna6pY5u45VIdYaX3/view?usp=sharing)


## Presenters

Dr Abhishek Dutta, Prasanna Sridhar, Dr Weidi Xie, Professor Alison Noble, Professor Andrea Vedaldi, Professor Andrew Zisserman, Professor Dima Damen