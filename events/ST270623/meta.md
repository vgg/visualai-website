## Agenda

**10.00 - 13.00**: Show & Tell Presentations

**13.00 - 14.00**: Lunch & Networking

## Location
Lecture Theatre B.H05 \
Humanities Building \
Arts Complex \
Woodland Rd \
Bristol BS8 1TB

[Google Maps](https://goo.gl/maps/cA2HjzXzvmBzAZxX6)

## Contact

[Contact the Organiser](mailto:visualai.uk@gmail.com)