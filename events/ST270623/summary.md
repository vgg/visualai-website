## Description

Do you use visual data in your research?—Images, Medical Images, Videos or Aerial Footage?


We offer an open source, tried and tested, suite of software tools to analyse, describe and search image and video
content that is easy to use, supported by research engineers at the University of Oxford, to help you access and
analyse your data, and use machine learning algorithms, efficiently and reliably.


Our research suite of software tools is web-based requiring no installation and work on any device. They allow
collaborative projects so multiple researchers can work together and has been successfully used in various
disciplines ranging from fish counting in videos to DEXA images and scanned ancient manuscripts.


During this Show and Tell, we will provide detailed walk-throughs of various open-source software as well as recent and
upcoming software for research collaborations.


Visual AI is a 5-Year EPSRC Program Grant, led by Prof Andrew Zisserman, University of Oxford in collaboration with
the University of Bristol, Prof Dima Damen (Co-I) and the University of Edinburgh. Our goal is to transfer the latest
computer vision methods to industry and to other academic disciplines (such as Archaeology, Art, Geology,
Medicine, Plant sciences and Zoology).  The Visual AI researchers and software engineers are thus excited to be
bringing this Show and Tell to the University of Bristol for this one-off event in search of collaborators where we
can continue to make impact.



## Presentations

[Introduction - Andrew Zisserman and Alison Noble](events/ST270623/data/introduction.pdf)
[Tracking in Video - Andrea Vedaldi](https://drive.google.com/file/d/1AVWiG1czYeggE-MKhWEX73n4mF638-PQ/view?usp=sharing)
[Tracking in Video with VVT - Prasanna Sridhar](events/ST270623/data/tracking_prasanna.pdf)
[Follow Things Around demo - David Pinto](events/ST270623/data/fta_david.pdf)
[VIA - Image and Video Annotation - Abhishek Dutta](events/ST270623/data/via-adutta.pdf)
[Image Comparison - Prasanna Sridhar](events/ST270623/data/prasanna_imagecompare.pdf)
[Image Search and Retrieval - Andrew Zisserman](events/ST270623/data/introduction_to_image_and_video_search_az.pdf)
[WISE Image Search Engine (WISE) demo - Horace Lee](events/ST270623/data/wise_horace.pdf)
[VGG Image Search Engine (VISE) demo - Abhishek Dutta](events/ST270623/data/vise_adutta.pdf)
[Applications in Zoology - Dan Schofield](events/ST270623/data/dan_applications_zoology.pdf)

## Presenters

Dr. Giles Bergel, Prof. Dima Damen, Dr. Abhishek Dutta, Horace Lee, Prof. Alison Noble, Dr. David
Pinto, Dr. Dan Schofield, Prasanna Sridhar, Prof. Andrea Vedaldi, Prof. Andrew Zisserman