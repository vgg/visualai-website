#	Visual AI Website Structure

##	Directories 
Location  | About
----- | ----
/bibtex | Includes bibtex file for publications 
/events | Includes markdown and data for events page
/favicons | Includes URL icons
 /images	|	Includes images of Software Demos,Software,Investigators,Researchers,Events,Publications,Themes,Footer,Homepage background
 /logos	|	Includes images of partners (used in index.html and partners.html)
 /markdown	|	Includes markdown files directly rendered in website 
/public	|	Includes Visual AI Header logo and Icon Font file (used in header)
/scripts	|	Includes javascript files used in website
/stylesheets	|	Includes css files used in website


##	Files loaded from external Sources
* 'Roboto' font from google fonts
* 'Roboto Slab' font from google fonts

##	Frameworks,libraries or plugins
Purpose | Location  | Link
---- | ---- | ----
Markdown Rendering	|	/scripts/remarkable.min.js | 	http://github.com/jonschlinkert/remarkable
Bibtex Parser | /publications.html | code.google.com/p/bibtex-js

## Guidelines for changing markdown files
* Refer to markdown syntax from https://daringfireball.net/projects/markdown/syntax
* Preview markdown files at http://jonschlinkert.github.io/remarkable/demo/
* Changes in Markdown files are directly reflected (no processing or compilation is required)
* Markup Structure of Markdown files should remain same.

## Ideal Dimensions for images
Section | Aspect Ratio (w:h)
--- | ---
Demos  | 2:1  
Software | 4:3
Investigators | 1:1
Researchers | 1:1
Partners  | t:1  (t<4)
Publications | 4:3
Events | 1:1


