#### Programme Manager

- # Dr. Sarah Clayton

  [![](images/researchers/sarah.jpg)](mailto:removethisifyouarehuman-sarah.clayton@eng.ox.ac.uk)

  Sarah is the Programme Manager for the Visual Geometry Group (VGG) at the
  University of Oxford. Sarah holds a PhD in Cognitive Psychology and moved from
  researching children’s mathematical development to focus her career on research
  coordination in 2019. Before joining VGG, Sarah was a Research Manager in the
  departments of Experimental Psychology and Psychiatry at the University of Oxford.
  Sarah supports the VisualAI project with recruitment, budgeting, project reporting,
  event organisation, and all other operational activities.

#### Snr. HPC Systems Administrator

- # Dr. Ashish Thandavan

  [![](images/researchers/ash.jpg)](mailto:removethisifyouarehuman-ashish@robots.ox.ac.uk)

  Ashish Thandavan is a Senior HPC System Administrator at VGG, University of Oxford, working on the VisualAI project. With a keen interest in High Performance Compute and Storage systems, Ashish is responsible for VGG's research infrastructure, comprising GPU and compute clusters, networks and storage systems. He was previously a Linux Systems Administrator at the Department of Computer Science at the University of Oxford, and an HPC Systems Administrator before that at the University of Reading. He received his PhD in Grid Computing and Workflows from the University of Reading in 2008.

#### Snr. Research Software Engineers

- # Dr. Abhishek Dutta

  [![](images/researchers/abhishek.jpg)](https://abhishekdutta.org/)

  Abhishek is a Senior Research Software Engineer in the Visual Geometry Group (VGG) of the Department of Engineering Science at Oxford University. After receiving his doctorate at the University of Twente (Netherlands) in 2015, he joined the Product Lab of TomTom International as a Senior Engineer. Abhishek obtained his Bachelor’s degree in Computer Engineering from the Tribhuvan University (Nepal) in 2009 and MSc in Computer Science (by research) from the University of York (UK) in 2010. More details about Abhishek and his work are available at his personal website: [https://abhishekdutta.org/](https://abhishekdutta.org/).

- # Dr. David Miguel Susano Pinto

  [![](images/researchers/pinto.jpg)](https://carandraug.net)

  David is a Senior Research Software Engineer in the Visual Geometry Group (VGG) of the Department of Engineering Science at the University of Oxford. He studied a degree in Biology and earned a PhD in Biochemistry through reproducible research in histone sequences and chromatin live-cell imaging before. He later joined Micron Oxford as image analysis specialist where he worked on the Python-Microscope and Cockpit python packages with Prof. Ian Dobbie. David is a developer of the GNU Octave programming language and has been involved in multiple free and open source software projects. He sometimes writes at [https://carandraug.net](https://carandraug.net).

#### Research Software Engineers

- # Horace Lee

  [![](images/researchers/horace.png)](#)
  Horace is a Research Software Engineer in the Visual Geometry Group (VGG) of the Department of Engineering Science at the University of Oxford.
  Before joining the group, he received his MEng in Electronic and Information Engineering from Imperial College London, where he wrote a Master’s thesis on adversarial robustness in machine learning. Previously, he worked as an intern Machine Learning Engineer at a London-based startup, where he worked on object detection and related problems.

- # Prasanna Sridhar

  [![](images/researchers/Prasanna.jpg)](#)
  Prasanna is a Research Software Engineer in the Visual Geometry Group (VGG) of the Department of Engineering Science at the University of Oxford.
  He received his BTech. from the Indian Institute of Technology Madras in 2016. While studying for his bachelor's degree in Mechanical Engineering, he was drawn towards the field of Computer Vision and Deep Learning, and decided to join an early stage Deep Learning startup in India as a Software Engineer after graduation. Since then, He has gained professional experience working at the intersection of Deep Learning, Infrastructure and Software development.

#### Project Researchers

- # Dr. Tengda Han

  [![](images/researchers/tengda.jpg)](#)

  Tengda is a postdoctoral research fellow in the Visual Geometry Group. Tengda received his PhD from the University of Oxford supervised by Andrew Zisserman. His current research interests are video understanding and self-supervised learning.

- # Dr. Michael Wray

  [![](images/researchers/MichaelWray.jpg)](#)

  Michael is an Assistant Professor (Lecturer) in Computer Vision at the University of Bristol. Mike received his PhD from the University of Bristol in 2019 supervised by Dima Damen and Walterio Mayol Cuevas. His research focus is action recognition in video and how both vision and language can be tied together for improved and scalable approaches.

- # Dr. Bin Zhu

  [![](images/researchers/bin_zhu.jpg)](#)

  Bin Zhu is a Research Associate in Computer Vision at the University of Bristol. Bin received his PhD in computer science from the City University of Hong Kong in 2021 supervised by Prof. Chong-Wah Ngo and Dr. Wing-Kwong Chan. His research interests include video understanding and multi-modal learning.

- # Dr. Toby Perrett

  [![](images/researchers/toby_perrett.png)](#)

  Toby Perrett is a Senior Research Associate in Computer Vision at the University of Bristol. Toby received his PhD from the University of Bristol in 2018. His research focus is challenges in video understanding under few-shot, long-tail and domains adaptation/generalisation regimes.

- # Dr. Octave Mariotti

  [![](images/researchers/octave.jpg)](#)

  Octave is a postdoctoral research associate in the visual computing group at the University of Edinburgh. He obtained his PhD from the same group under the supervision of Dr. Hakan Bilen, with a focus on unsupervised object pose estimation. His research interests cover learning with limited human supervision, discovering the structure of the visual world, and robust evaluation of modern deep learning methods.

- # Dr. Xiaoqing Guo

  [![](images/researchers/xiaoqing.jpg)](#)

  Xiaoqing is a postdoctoral research fellow in the Biomedical Image Analysis Group at the University of Oxford, advised by Prof Alison Noble. She received her PhD in Department of Electrical Engineering from City University of Hong Kong in 2022 and BEng in Department of Biomedical Engineering from Beihang University in 2018. Her current research interest is multi-modal learning with vision, text, audio, and gaze.

- # Dr. Vladimir Iashin

  [![](images/researchers/vladimir.jpeg)](#)

  Vladimir is currently a postdoctoral research assistant within the Visual Geometry Group. He obtained his PhD from Tampere University. Vladimir's primary research pursuits revolve around the domain of multi-modal video understanding.

- # Dr. Kranti Kumar Parida

  [![](images/researchers/kranti.jpeg)](#)

  Kranti Kumar Parida is a Research Associate in Multimodal Video Understanding at the University of Bristol. Kranti received his PhD from the Indian Institute of Technology Kanpur, India, in 2023, supervised by Dr. Gaurav Sharma and Prof. Manindra Agrawal. His research focus is in the broad area of audio-visual understanding, particularly efficient and effective combination of both modalities for different tasks.

#### Project Ambassadors

- # Dr. Giles Bergel

  [![](images/researchers/giles.jpg)](#)

  Giles Bergel is a Senior Researcher in Digital Humanities. A book
  historian by training, he received his PhD from Queen Mary, University of London,
  after which he worked on projects to digitise historic printed ballads at the
  Universities of California, Santa Barbara and Oxford, while teaching book history.
  He instigated and managed Broadside Ballads Online for the Bodleian Library,
  which incorporates computer vision tools, and continues to research the
  history of printing.

- # Dr. Daniel Schofield

  [![](images/researchers/dan.jpg)](#)

  Daniel Schofield is a postdoctoral researcher at the University of Oxford with a background in animal behaviour and anthropology. He received his DPhil from the University of Oxford under the supervision of Prof. Susana Carvalho (Anthropology) and Prof. Dora Biro (Zoology). For his research he collaborated with the Visual Geometry Group to pioneer the application of Visual AI for the analysis of primate social behaviour from large-scale video archives collected in the wild.

#### Former Project Members

- # Dr. Rebecca Bryant

  [![](images/researchers/rebecca.jpg)](mailto:removethisifyouarehuman-rebecca.bryant@eng.ox.ac.uk)

  Rebecca was the Programme Manager for the Visual Geometry Group in 2023. She has had a long and varied career in the research, university and academic publishing sectors, starting out in 1997 as a postdoctoral researcher in philosophy and then moving on to hold management roles across a number of different organisations including Oxford University Press, Oxford Brookes University and the University of Oxford. She has worked at the University of Oxford since 2010 and has a particular interest in research ethics, having held the position of Research Ethics Manager within Research Services for seven years. She also spent several years running her own writing and editorial business. Rebecca received her PhD in Philosophy from the University of Edinburgh in 1997. She also holds a Postgraduate Diploma in Law (College of Law, 1994) and an MA Hons. in Philosophy (University of Edinburgh, 1993).

- # Jenny Hu

  [![](images/researchers/jenny.jpg)](mailto:removethisifyouarehuman-jenny.hu@eng.ox.ac.uk)

  Jenny was the programme manager for Visual AI until October 2022. The focus of her role was on managing the operational activities linked to the research objectives of the programme, including but not limited to staff and student recruitment, financial budgeting and expenditure, project reporting and auditing, internal and external event organisation and, liaising with other stakeholders such as EPSRC and the Advisory Board on programme related administrative matters.

- # Dr. Jianbo Jiao

  [![](images/researchers/Jianbo.jpg)](#)

  Jianbo is an assistant professor at the University of Birmingham. Previously, he was a Postdoctoral Researcher in the Biomedical Image Analysis Group and the Visual Geometry Group at the University of Oxford, advised by Prof. Alison Noble and Prof. Andrew Zisserman. He obtained his PhD in Computer Science from City University of Hong Kong in 2018. His research interests include Computer Vision and Machine Learning. He is currently mainly focusing on self-supervised learning and multimodal learning.

- # Dr. Weidi Xie

  [![](images/researchers/Weidi.png)](#)

  Weidi Xie is an associate professor at Shanghai Jiao Tong University. Previously, he was a senior postdoctoral researcher at Visual Geometry Group. His research interest is on Self-supervised Learning, Video Understanding, General Purpose Vision Systems.

- # Dr. Samuel Albanie

  [![](images/researchers/SamAlbanie.png)](#)

  Samuel is an assistant professor at the University of Cambridge. Previously, he was a senior researcher in Visual AI in the Visual Geometry Group and a Research Fellow in the Sciences at Balliol College, Oxford. He received his PhD from the University of Oxford in 2019. His research spans topics in computer vision and machine learning, with a particular focus on learning from limited supervision using multi-modal sensory data.
