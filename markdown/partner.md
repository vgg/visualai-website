#### Partners

*   # PUBLIC SERVICE
	[![](logos/bbc.png "BBC R&D")](https://www.bbc.co.uk)
*   # INDUSTRY
	[![](logos/nielsen.png "Nielsen")](http://www.nielsen.com)
	[![](logos/toshiba.png "Toshiba Corporation")](http://www.toshiba.com)
	[![](logos/iu.png "Intelligent Ultrasound Ltd")](http://www.intelligentultrasound.com)
	[![](logos/sr.png "Samsung AI Centre Cambridge")](https://research.samsung.com/aicenter_cambridge)
	[![](logos/continental.png "Continental Teves AG & Co. oHG")](https://www.continentaltevesukltd.co.uk/)
	[![](logos/plexalis.svg "Plexalis Teves AG & Co. oHG")]()