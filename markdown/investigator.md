####	Project Investigators

*   # Prof. Andrew Zisserman

    [![](images/inv1.jpg)](http://www.robots.ox.ac.uk/~az)

    Prof. Andrew Zisserman, FRS, is the Principal Investigator for Visual
	AI. He is a Royal Society Research Professor and the Professor of
	Computer Vision Engineering at the Department of Engineering Science
	at the University of Oxford. He is the founder of the Visual Geometry
	Group (VGG). He is known internationally for his pioneering work in
	multiple view geometry, visual recognition, and large scale retrieval
	in images and video. He has authored over 450 peer reviewed papers in
	computer vision, and coedited and written several books in this
	area. His papers have won many best paper and test-of-time awards at
	international conferences, including the IEEE
	Marr prize three times. He regularly serves as programme chair or
	general chair for international computer vision conferences.  In 2013
	he was awarded the IEEE PAMI Distinguished Researcher award, and in
	2017 the Royal Society Milner award.  He is a Fellow of the Royal
	Society.


*   # Prof. Alison Noble

    [![](images/inv2.png)](http://www.ibme.ox.ac.uk/research/biomedia/people/professor-alison-noble)

	Professor Alison Noble OBE FREng FRS is the University of Oxford Technikos Professor of Biomedical Engineering and a former Associate Head of the MPLS Division and Director of the Oxford Institute of Biomedical Engineering. Alison is best known for her group’s research on machine-learning based ultrasound image analysis much of which has involved interdisciplinary collaborators with clinical partners. She received the MICCAI Society Enduring Impact Award in 2019, and the Royal Society Gabor Medal in the same year. She is a European Research Council Advanced Research award holder leading a multi-modal ultrasound video analysis project (PULSE). Other research is developing low-cost ultrasound embedded-AI devices for pregnancy risk assessment in low-and-middle-income-country settings.  She co-founded Intelligent Ultrasound Ltd in 2012, which has gone on to develop and commercialise software-based tools to assist sonographers in workflow and quality assurance. Alison is a Fellow of Royal Academy of Engineering (2008) and a Fellow of the Royal Society (2017). She is a Trustee of the Oxford Trust and was awarded an OBE for services to science and engineering in 2013.


*   # Prof. Andrea Vedaldi

    [![](images/inv3.jpg)](http://www.robots.ox.ac.uk/~vedaldi//)

	Andrea Vedaldi is Professor of Computer Vision and Machine Leanring at the University of Oxford, where he co-leads the Visual Geometry Group since 2012. He is also a research scientist in Facebook AI Research (FAIR) in London. His recent work has focused on unsupervised learning of representations and geometry in computer vision. He is author of more than 130 peer-reviewed publications in the top machine vision and artificial intelligence conferences and journals. He is a recipient of the Mark Everingham Prize for selfless contributions to the computer vision community, the Open Source Software Award by the ACM, and the best paper award from the Conference on Computer Vision and Pattern Recognition.

*   # Prof. Dima Damen

    [![](images/inv4.jpg)](http://people.cs.bris.ac.uk/~damen/)

	Dima Damen is a Professor of Computer Vision at the University of Bristol. Dima is currently an EPSRC Fellow (2020-2025), focusing her research interests in the automatic understanding of object interactions, actions and activities using wearable visual (and depth) sensors. She has contributed to novel research questions including assessing action completion, skill/expertise determination from video sequences, discovering task-relevant objects, dual-domain and dual-time learning as well as multi-modal fusion using vision, audio and language. She is the project lead for EPIC-KITCHENS, the largest dataset in egocentric vision, with accompanying open challenges. She also leads the EPIC annual workshop series alongside major conferences (CVPR/ICCV/ECCV). Dima is a program chair for ICCV 2021, associate editor of IJCV, IEEE TPAMI and Pattern Recognition. She was selected as a Nokia Research collaborator in 2016, and as an Outstanding Reviewer in CVPR2021, CVPR2020, ICCV2017, CVPR2013 and CVPR2012. Dima received her PhD from the University of Leeds (2009), joined the University of Bristol as a Postdoctoral Researcher (2010-2012), Assistant Professor (2013-2018), Associate Professor (2018-2021) and was appointed as chair in August 2021. She supervises 9 PhD students, and 5 postdoctoral researchers.

*   # Dr. Hakan Bilen

    [![](images/inv5.jpg)](http://homepages.inf.ed.ac.uk/hbilen/)

    Hakan Bilen is an Assistant Professor in the School of Informatics in the University of Edinburgh. Bilen joined the Institute of Perception, Action and Behaviour (IPAB) at Edinburgh in 2017 and is leading several computer vision and machine learning projects in the institute. His core expertise is in computer vision and machine learning with a focus on weakly supervised learning, multi-task learning and video classification. Specifically his research focuses on learning to understand images with minimal human supervision and learning to perform multiple tasks jointly. He is co-author of more than 20 publications in the major international conferences and journals. He has been an area chair for ECCV 2018, BMVC 2018–19, CVPR 2021, ICCV 2021 and a regular reviewer for all the major computer vision and machine learning conferences and journals. He is an active member of international computer vision community and organized workshops and tutorials at CVPR 2017–18, ECCV 2020. He received the best paper award in BMVC 2011 and outstanding reviewer award in ICCV 2017.
