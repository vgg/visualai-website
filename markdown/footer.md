
*	## Funding

	Visual AI is funded by the EPSRC (Engineering and Physical Research Council) Programme Grant EP/T028572/1.
	
*	## Quick Links

	[Visual Geometry Group](https://www.robots.ox.ac.uk/~vgg/)
	[Faculty of Engineering, Bristol](https://www.bristol.ac.uk/engineering/)
	[School of Informatics, Edinburgh](https://www.ed.ac.uk/informatics)
	[Department of Engineering Science, Oxford](https://www.eng.ox.ac.uk/)
	[Engineering and Physical Sciences Research Council](https://www.epsrc.ac.uk/)
	

  
* ![](images/oxford.png)
* ![](images/uoe.png)
* ![](images/bristol.png)
* ![](images/epsrc.png)



[visualai.uk@gmail.com](mailto:visualai.uk@gmail.com)
